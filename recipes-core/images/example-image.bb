DESCRIPTION = "Example image based on ttc-image-clutter which adds a lightweight" \
              "http-server to the filesystem"

require recipes-core/images/ttc-image-clutter.bb

IMAGE_INSTALL += " \
    lighttpd \
"
